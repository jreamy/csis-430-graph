# Graph Algorithms

This project contains the graph that I wrote for the CSIS 430 Algorithms course, displaying the algorithmic speed of my Branch and Bound algorithm.  

## Traveling Salesman Problem.

Many of the algorithms developed in this project are for the Traveling Salesman Problem: searching a graph for the 'cheapest' traversal.  

### Branch and Bound

The [Branch and Bound](src/main/scala/graph.scala#L1298) Algorithm produces the fastest, provably correct solution to the Traveling Salesman Problem.  For context, I ran my implementation on my machine in 2 minutes whereas other students' implementations took up to 2 hours to run on the same graph.  I designed and included a dynamic speedup of the standard branch and bound algorithm adding an extra O(n) speedup to the NP-Hard O(n^2 * 2^n) implementation.  Branch and bound uses the output of a fast Genetic Algorithm as its initial 'best' solution, allowing more paths to be ignored.

### Genetic Algorithm

The [Genetic Algorithm](src/main/scala/graph.scala#L1108) is the [Tao and Michalewicz' Inver-Over algorithm](http://dl.acm.org/citation.cfm?id=668606).  Successive generations are compared with their offspring generated either by mutation or cross-over, and replaced if the offspring are better.

### Modified Kruskal's

Additionally, I modified the Kruskal minimum spanning tree to output a solution to the Traveling Salesman Problem.  [The algorithm](src/main/scala/graph.scala#L863) is very fast, but generally does worse than the basic 2-opt algorithm.

## The Graph

My [graph](src/main/scala/graph.scala#L347) is essentially a sparse matrix using Hashing.  Lookups and additions of edges and vertices are thus O(1).  Removing a vertex is max O(n).

### Graph Correctness Tests

I also wrote a [suite of tests](src/test/scala) that verify the correctness of the graph implementation.
