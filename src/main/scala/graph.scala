
import scala.collection.immutable.HashMap
import scala.collection.immutable.HashSet
import scala.collection.mutable.Queue
import scala.collection.mutable.Stack
import scala.collection.mutable.{HashMap => MutableHashMap}
import scala.util.Random
import scala.xml.XML.loadFile
import scala.xml.Node
import java.io.IOException
import java.util.Scanner
import java.io.File


/**
 * Contains the Graph object and its dependencies.
 * @author jreamy17@georgefox
 */
object graph
{
    /**
     * A trait for representing directed and undirected graphs
     * @tparam T the type of the vertices of the graph
     */
    trait Graph[T]
    {
        /**
         * Returns whether the graph is directed or not.
         *
         * @return whether the graph is directed or not
         */
        def isDirected:Boolean

        /**
         * Returns true if the graph contains no vertices or edges.
         *
         * @return true if the graph contains no vertices or edges
         */
        def isEmpty:Boolean

        /**
         * Returns the number of vertices in the graph.
         *
         * @return the number of vertices in the graph
         */
        def sizeVertices:Int

        /**
         * Returns true if the graph contains the vertex
         *
         * @return true if the graph contains the edge
         */
        def vertexExists(vertex:T):Boolean

        /**
         * Returns an Iterable of all the vertices in the graph.
         *
         * @return an Iterable of all the vertices in the graph
         */
        def getVertices:Iterable[T]

        /**
         * Returns an Iterable containing all vertices adjacent to the
         *   given vertex.
         *
         * @return an Iterable containing all vertices adjacent to the
         *   given vertex
         * @throws IllegalArgumentException if the given vertex is null or
         *   not in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def getAdjacent(source:T):Iterable[T]

        /**
         * Returns a new copy of the graph with the given vertex added.
         *
         * @return a new copy of the graph with the given vertex added
         * @throws IllegalArgumentException if the given vertex is
         *   invalid or already in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def addVertex(vertex:T):Graph[T]

        /**
         * Adds multiple vertices at once to the graph.
         *
         * @return a new copy of the graph containing the added vertices
         * @throws IllegalArgumentException if any given vertex is null or
         *   already in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def addVertices(vertices:T*):Graph[T]

        /**
         * Returns a new copy of the graph with the given vertex removed.
         *
         * @return a new copy of the graph with the given vertex removed
         * @throws IllegalArgumentException if the given vertex is null or not
         *   in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def removeVertex(vertex:T):Graph[T]

        /**
         * Returns the number of edges in the graph.
         *
         * @return the number of edges in the graph
         */
        def sizeEdges:Int

        /**
         * Returns true if an edge exists between the vertices.
         *
         * @return true if an edge exists between the vertices
         */
        def edgeExists(source:T, destination:T):Boolean

        /**
         * Returns an Iterable of all the edges in the graph.
         *
         * @return an Iterable of all the edges in the graph
         */
        def getEdges:Iterable[Edge[T]]

        /**
         * Returns the weight of the edge between the given vertices.
         *
         * Returns -1 if there is no edge between the vertices.
         *
         * @return the weight of the edge between the given vertice
         *   or -1 if no edge exists
         */
        def getEdgeWeight(source:T, destination:T):Int

        /**
         * Gets the edge object between two given vertices, if one exists.
         *
         * @return Option containing the edge between two vertices,
         * `None` if no such edge exists
         * @throws IllegalArgumentException if the vertices are invalid
         */
        @throws(classOf[IllegalArgumentException])
        def getEdge(source:T, destination:T):Option[Edge[T]]

        /**
         * Returns a new copy of the graph with the given edge added.
         *
         * @return a new copy of the graph with the given edge addEdge
         * @throws IllegalArgumentException if the given vertices are
         *   invalid (null or not in the graph) or if the given weight
         *   is invalid (negative)
         */
        @throws (classOf[IllegalArgumentException])
        def addEdge(source:T, destination:T, weight:Int):Graph[T]

        /**
         * Adds multiple edges at once to the graph.
         *
         * @return a new copy of the graph containing the added edges
         * @throws IllegalArgumentException if any given edge is already in the
         *    graph or is negative, or any vertices are null or not in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def addEdges(edges:(T,T,Int)*):Graph[T]

        /**
         * Returns a new copy of the graph with the given edge removed.
         *
         * @return a new copy of the graph with the given edge removed
         * @throws IllegalArgumentException if the given vertices are null or
         *   not in the graph, or if the given edge not in the graph
         */
        @throws(classOf[IllegalArgumentException])
        def removeEdge(source:T, destination:T):Graph[T]

        /**
         * Returns the length of the path given by the vertices in the input
         *   sequence.
         *
         * Returns None if the path does not exist in the graph.
         *
         * @return the length of the input path
         */
        def pathLength(path:Seq[T]):Option[Long]

        /**
         * Returns the shortest path between the given vertices given by
         *   Dijkstra's algorithm.
         *
         * @return the shortest path between the vertices or None if
         *   no path exists
         * @throws IllegalArgumentException if the input vertices are
         *   not in the graph or are null
         */
        @throws(classOf[IllegalArgumentException])
        def shortestPathBetween(source:T, destination:T):Option[Seq[Edge[T]]]

        /**
         * Returns the minimal spanning tree of the graph. None if there
         *   is no such tree
         * @return an option containing the minimum spannning tree of
         *   the graph, or none if there is no such tree
         */
        def minimumSpanningTree:Option[Graph[T]]

        def getKruskalTSP():Seq[Edge[T]]

        def getKruskalTSP(skip:Int = 0, shuffle:Double = 0.0):Seq[Edge[T]]

        /**
         * Creates a tour of the graph, then finds a local solution to the
         *   Traveling Salesperson Problem given the initial tour using 2-opt.
         *
         * @return a locally minimal tour of the graph
         */
        def getLocalTSP():Seq[Edge[T]]

        /**
         * Finds a local solution to the Traveling Salesperson Problem
         *  given some initial tour using 2-opt.
         *
         * @return a locally minimal tour of the graph
         */
        def getLocalTSP(initialTour:Seq[T]):Seq[Edge[T]]

        /**
         * Creates a tour of the graph, then finds a local solution to the
         *   Traveling Salesperson Problem given the initial tour using 2-opt.
         *
         * Uses a "beam" of the top-k tours to search for better solutions.
         *
         * @return a locally minimal tour of the graph
         */
        def getBeamTSP(beamSize:Int):Seq[Edge[T]]

        /**
         * Finds a local solution to the Traveling Salesperson Problem
         *  given some initial tour using 2-opt.
         *
         * Uses a "beam" of the top-k tours to search for better solutions.
         *
         * @return a locally minimal tour of the graph
         */
        def getBeamTSP(initialTour:Seq[T], beamSize:Int):Seq[Edge[T]]

        /**
         * Returns the best tour found by random mutation.
         *
         * The original algorithm is the
         *   [[http://dl.acm.org/citation.cfm?id=668606 Tao and Michalewicz'
         *   Inver-Over algorithm]].  Successive generations are compared
         *   with their offspring generated either by mutation or cross-over,
         *   and replaced if the offspring are better.
         *
         * Mutation occurs within a selected tour.  Two vertices are chosen
         *   at random and the vertices between them are reversed.
         *
         * Cross-over occurs between two tours.  One vertex is chosen at
         *   random from the given tour, another tour is selected, and the
         *   selected vertex is found, along with the corresponding 'neighbor'.
         *  The given neighbor is found in the inital tour, and the vertices
         *   are reversed so that the vertex and neighbor are adjacent.
         *
         * @return the best tour found by random mutation
         */
        def getGeneticTSP(popSize:Int, inversionProb:Float, maxIters:Int):
            Seq[Edge[T]]

        /**
         * Returns the best tour found by random mutation.
         *
         * The original algorithm is the
         *   [[http://dl.acm.org/citation.cfm?id=668606 Tao and Michalewicz'
         *   Inver-Over algorithm]].  Successive generations are compared
         *   with their offspring generated either by mutation or cross-over,
         *   and replaced if the offspring are better.
         *
         * Mutation occurs within a selected tour.  Two vertices are chosen
         *   at random and the vertices between them are reversed.
         *
         * Cross-over occurs between two tours.  One vertex is chosen at
         *   random from the given tour, another tour is selected, and the
         *   selected vertex is found, along with the corresponding 'neighbor'.
         *   The given neighbor is found in the inital tour, and the vertices
         *   are reversed so that the vertex and neighbor are adjacent.
         *
         * @return the best tour found by random mutation
         */
        def getGeneticTSP:Seq[Edge[T]]

        /**
         * Finds the global solution to the Traveling Salesperson Problem via
         *   an exhaustive search of solutions using a dynamic implementation.
         *
         * @return a sequence of edges that is the global minimal tour of
         *   the graph
         */
        def dynamicTSP:Seq[Edge[T]]

        /**
         * Finds the global solution to the Traveling Salesperson Problem via
         *   exhaustively searching the graph for all solutions, pruning any
         *   paths that are more expensive than previously visisted solutions.
         *
         * @return a sequence of edges that is the global minimal tour of
         *   the graph
         */
        def branchBoundTSP:Seq[Edge[T]]

        override def toString:String
    }

    class Edge[T](val source:T, val destination:T, val weight:Int)
        extends Ordered[Edge[T]]
    {
        def compare(that:Edge[T]):Int =
        {
            this.weight.compare(that.weight)
        }

        override def equals(arg0:Any):Boolean =
        {
            if (arg0.isInstanceOf[Edge[T]])
            {
                // Cast
                val that = arg0.asInstanceOf[Edge[T]]

                // Compare
                val src = source.equals(that.source)
                val dst = destination.equals(that.destination)
                val wgh = weight.equals(that.weight)

                src && dst && wgh
            }
            else false
        }

        override def toString:String =
        {
            "Edge(" + source + ", " + destination + ", " + weight + ")"
        }
    }

    /**
     * An immutable class for representing directed and undirected graphs
     */
    object Graph
    {
        /**
         * Creates and returns a new empty Graph - acts as a constructor
         */
        def apply[T](isDirected:Boolean):Graph[T] =
        {
            new GraphImpl[T](
                isDirected,
                new HashMap[GraphEdge[T], Int],
                new HashSet[T]
            )
        }

        /**
         * Reads a Graph from file.
         *
         * @return the graph contained in the file
         * @throws IOException if the file read contains any invalid
         *   formatting
         */
        @throws(classOf[IOException])
        def fromCSVFile(isDirected:Boolean, fileName:String):Graph[String] =
        {
            // Shorthand exit
            def quit(message:String) = throw new IOException(message)

            // Graph to return
            var gr = Graph[String](isDirected)

            // Scanners used
            val sc:Scanner = new Scanner(new File(fileName))
            var line:Scanner = null

            // Get the number of vertices
            val numVerts = if (sc.hasNextInt) sc.nextInt
                else quit("Invalid Number Vertices")

            // Clear the line character
            sc.nextLine

            // Add all the vertices
            for (n <- 0 until numVerts)
            {
                gr = if (sc.hasNext) gr.addVertex(sc.nextLine)
                    else quit("Invalid Vertex")
            }

            // Get the number of edges
            val numEdges = if (sc.hasNextInt) sc.nextInt
                else quit("Invalid Number Edges")

            // Clear the line character
            sc.nextLine

            // Change the delimeter
            sc.useDelimiter(",")

            // Add all the edges
            for (n <- 0 until numEdges)
            {
                // Get the next line from file
                line = new Scanner(sc.nextLine).useDelimiter(",")

                // Parse the line into source, destination, weight
                val source = if (line.hasNext) line.next
                    else quit("Invalid Vertex")
                val destination = if (line.hasNext) line.next
                    else quit("Invalid Vertex")
                val weight = if (line.hasNextInt) line.nextInt
                    else quit("Invalid Edge Weight")

                // Add the new edge
                try
                {
                    gr = gr.addEdge(source, destination, weight)
                }
                catch
                {
                    case ill: IllegalArgumentException => {
                        quit("Invalid Edge Inputs")
                    }
                }

                // Catch trailing
                if (line.hasNext) quit("Invalid Trailing Characters")
            }

            if (sc.hasNext) quit("Invalid Trailing Characters")

            // Return the graph
            gr
        }

        /*
		Loads a graph from a TSP file
		*/
		def fromTSPFile(fileName:String):Graph[Int] =
		{
			//create an empty graph
			val emptyGraph = Graph[Int](false)

			//load the XML file
			val tspXML = loadFile(fileName)

			//get all the veritices
			val vertices = tspXML \\ "vertex"

			//add in all the vertices
			val graph = Range(0, vertices.size).foldLeft(emptyGraph)(
                (g,v) => g.addVertex(v)
            )

			//add in all the edges - they are part of each xml vertex
			vertices.zipWithIndex.foldLeft(graph)(
                (g,t) => addXMLEdges(g, t._1, t._2)
            )
		}

		/*
		Add in edges assume the vertices exist
		*/
		private def addXMLEdges(graph:Graph[Int], xmlEdges:Node, start:Int):
            Graph[Int] =
		{
			//parse all the edges - tuples of (destination, weight)
			val edges = (xmlEdges \ "edge").map(
                e => (e.text.toInt, e.attributes("cost").text.toDouble.toInt)
            )

			//remove the edges that already exist
			val newEdges = edges.filterNot(e => graph.edgeExists(start, e._1))

			//add in new edges
			newEdges.foldLeft(graph)((g,e) => g.addEdge(start, e._1, e._2))
		}

        /**
         * Private implementation of the Graph class
         */
        private class GraphImpl[T] (
                val isDirected:Boolean,
                val matrix:HashMap[GraphEdge[T], Int],
                val indices:HashSet[T]
        ) extends Graph[T]
        {
            def isEmpty():Boolean =
            {
                indices.isEmpty && matrix.isEmpty
            }

            def sizeVertices:Int =
            {
                indices.size
            }

            def vertexExists(vertex:T):Boolean =
            {
                indices.contains(vertex)
            }

            def getVertices:Iterable[T] =
            {
                indices.toIterable
            }

            @throws(classOf[IllegalArgumentException])
            def getAdjacent(source:T):Iterable[T] =
            {
                //Validate input
                ensureNotNull(source)
                ensureContainsVertex(source)

                // Return vertices adjacent
                indices.filter((d) => {
                    matrix.contains(GraphEdge(source, d, isDirected))
                }).toIterable
            }

            @throws(classOf[IllegalArgumentException])
            def addVertex(vertex:T):Graph[T] =
            {
                // Validate input
                ensureNotNull(vertex)
                ensureNotContainsVertex(vertex)

                // Return new graph
                new GraphImpl[T](isDirected, matrix, indices + vertex)
            }

            @throws(classOf[IllegalArgumentException])
            def addVertices(vertices:T*):Graph[T] =
            {
                // Validate inputs
                for (v <- vertices)
                {
                    ensureNotNull(v)
                    ensureNotContainsVertex(v)
                }

                // Return new graph
                new GraphImpl[T](isDirected, matrix, indices ++ vertices)
            }

            @throws(classOf[IllegalArgumentException])
            def removeVertex(vertex:T):Graph[T] =
            {
                // Validate input
                ensureNotNull(vertex)
                ensureContainsVertex(vertex)

                // Make new matrix
                val newMatrix = matrix.filterNot((k) => k._1.contains(vertex))

                // Return new graph
                new GraphImpl[T](isDirected, newMatrix, indices - vertex)
            }

            def sizeEdges():Int =
            {
                matrix.size
            }

            def edgeExists(source:T, destination:T):Boolean =
            {
                // Protect from nulls
                if (containsNull(source, destination)) false
                else
                {
                    // Edge to search for
                    val edge = GraphEdge[T](source, destination, isDirected)

                    // Check backing for edge
                    matrix.contains(edge)
                }
            }

            def getEdges():Iterable[Edge[T]] =
            {
                matrix.toIterable.map(e => new Edge(e._1.x, e._1.y, e._2))
            }

            /**
             * Private function for converting Iterables of vertices to
             *  Iterables of the edges between the vertices.
             */
            private[this] def getEdges(tour:Iterable[T]):Iterable[Edge[T]] =
            {
                tour.sliding(2).toIterable.map(
                    (e) => getEdge(e.head, e.tail.head).get
                )
            }

            def getEdgeWeight(source:T, destination:T):Int =
            {
                // Protect from nulls
                if (containsNull(source, destination)) -1
                else
                {
                    // Edge to search for
                    val edge = GraphEdge[T](source, destination, isDirected)

                    // Get edge weight, or return -1 if not found
                    matrix.getOrElse(edge, -1)
                }
            }

            def getEdge(source:T, destination:T):Option[Edge[T]] =
            {
                if (edgeExists(source, destination))
                {
                    Some(new Edge[T](
                        source, destination, getEdgeWeight(source, destination)
                    ))
                }
                else None
            }

            @throws(classOf[IllegalArgumentException])
            def addEdge(source:T, destination:T, weight:Int):Graph[T] =
            {
                // Validate inputs
                ensureNotNull(source, destination, weight)
                ensureNotSame(source, destination)
                ensureContainsVertex(source, destination)
                ensureGreaterThanZero(weight)

                // Build new matrix
                val edge = GraphEdge[T](source, destination, isDirected)
                val newMatrix = matrix + ((edge, weight))

                // Return new graph
                new GraphImpl[T](isDirected, newMatrix, indices)
            }

            @throws(classOf[IllegalArgumentException])
            def addEdges(edges:(T,T,Int)*):Graph[T] =
            {
                var gr:Graph[T] = this

                // Builds graph and checks each edge
                for (edge <- edges)
                {
                    gr = gr.addEdge(edge._1, edge._2, edge._3)
                }

                // Return new graph
                gr
            }

            @throws(classOf[IllegalArgumentException])
            def removeEdge(source:T, destination:T):Graph[T] =
            {
                // Validate inputs
                ensureNotNull(source, destination)
                ensureNotSame(source, destination)
                ensureContainsVertex(source, destination)
                ensureContainsEdge(source, destination)

                // Build edge to remove
                val edge = GraphEdge[T](source, destination, isDirected)

                // Return new graph
                new GraphImpl[T](isDirected, matrix - edge, indices)
            }

            def pathLength(path:Seq[T]):Option[Long] =
            {
                // Output variable
                var length:Long = 0;

                // Exit flag
                var isContinuous:Boolean = !(path == null) && !(path.isEmpty);

                // Location counter
                var loc = 0;

                // Iterate over path, so long as it's continuous
                while (isContinuous && (loc < path.length - 1))
                {
                    // Ensure there's an edge
                    if (edgeExists(path(loc), path(loc + 1)))
                    {
                        // Increment length
                        length += getEdgeWeight(path(loc), path(loc + 1))
                    }
                    else
                    {
                        isContinuous = false
                    }

                    // Increment Location counter
                    loc += 1
                }

                // Return Option of path length
                if (isContinuous) Some(length) else None
            }

            @throws(classOf[IllegalArgumentException])
            def shortestPathBetween(source:T, destination:T):
            Option[Seq[Edge[T]]] =
            {
                // Validate inputs
                ensureNotNull(source, destination)
                ensureContainsVertex(source, destination)

                // Data structures
                var parent = new HashMap[T, T]()
                var dist = new HashMap[T, Int]()
                var unvisited = indices

                // Comp variables
                var current = source
                var cost = 0

                // Exit flag
                var completed = current.equals(destination)

                // Output variable
                var path:List[Edge[T]] = List.empty[Edge[T]]

                // Initialize the lookup tables
                parent = parent + ((source, source))
                dist = dist + ((source, cost))

                // Iterate over all vertices or until reached destination
                while (!unvisited.isEmpty && !completed)
                {
                    // Mark current as visited
                    unvisited = unvisited - current

                    // Iterate over all adjacent nodes
                    for (child <- getAdjacent(current))
                    {
                        // Get the weight of the edge to the vertex
                        val edgeCost = getEdgeWeight(current, child)

                        // Check distance table for cost comparison
                        val newEntry = !dist.contains(child) &&
                                unvisited.contains(child)
                        val currentCost = dist.getOrElse(child, 0)
                        val newCost = cost + edgeCost
                        val cheaper = (newCost < currentCost)

                        // If this is a cheaper (or new) path update it
                        if (newEntry || cheaper)
                        {
                            // Update child in distance map
                            dist = dist + ((child, newCost))

                            // Update parent map
                            parent = parent + ((child, current))
                        }
                    }

                    // Find min dist in unvisited
                    val minDists = dist.filter((t) => unvisited.contains(t._1))
                    current = if (!minDists.isEmpty) minDists.minBy(_._2)._1
                        else source
                    cost = if (!minDists.isEmpty) dist(current) else 0

                    // Check if the end condition is reached
                    completed = current.equals(destination) || minDists.isEmpty
                }

                // Ensure it wasn't an early exit
                if (parent.contains(destination))
                {
                    // Build path following parent map
                    while (parent.contains(current) &&
                        !parent(current).equals(current)
                    )
                    {
                        // Get the previous vertex and edge weight
                        val prev = parent(current)
                        val weight = getEdgeWeight(prev, current)

                        // Add the edge to the front of the path
                        path = new Edge[T](prev, current, weight) +: path

                        // Shift back one
                        current = prev
                    }

                    // Convert to seqence and return in Option
                    Some(path.toSeq)
                }
                else None
            }

            /**
             * Returns the minimum spanning tree of the graph, given
             *  by Kruskal's algorithm, or None if no such tree exists
             *
             * @return the minimum spanning tree of the graph, or None
             *  if no such tree exists
             */
            def minimumSpanningTree:Option[Graph[T]] =
            {
                // Directed Graphs have no minumum spanning tree
                if (isDirected) None
                else if (isEmpty) None
                else
                {
                    // Get the edges (sorted by weight)
                    var edges = getEdges.toSeq.sorted

                    // Set up a graph empty of edges
                    val newMat = new HashMap[GraphEdge[T], Int]
                    var gr:Graph[T] = new GraphImpl[T](
                        isDirected, newMat, indices
                    )

                    // Set up a disjoint set for the groupings
                    val groups = new DisjointHashSet[T](indices)

                    // Set up a list of unvisited vertices
                    var unused = indices

                    // Iterate over all the edges
                    while (!edges.isEmpty)
                    {
                        // Pop the first (cheapest) edge
                        val edge = edges.head
                        edges = edges.tail

                        // Chop up the edge for ease of use
                        val src = edge.source
                        val dst = edge.destination
                        val wgh = edge.weight

                        // Check if the groups are separate
                        if (groups.find(src) != groups.find(dst))
                        {
                            // Union the groups
                            groups.union(src, dst)

                            // Add conjoining edge
                            gr = gr.addEdge(src, dst, wgh)

                            // Mark the vertices as used
                            unused = unused - (src, dst)
                        }
                    }

                    // If the graphs contain the same vertices
                    if (unused.isEmpty) Some(gr)
                    else None
                }
            }
            def getKruskalTSP():Seq[Edge[T]] =
            {
                getKruskalTSP(skip=0, shuffle=1.0)
            }

            def getKruskalTSP(skip:Int=0, shuffle:Double=1.0):Seq[Edge[T]] =
            {
                class _bucket[T](val vertex:T)
                {
                    var head:_bucket[T] = null
                    var tail:_bucket[T] = null
                    var count = 0

                    def connect(other:_bucket[T]) =
                    {
                        if (count == 0)
                            head = other
                        else
                            tail = other

                        count += 1
                    }
                }
                // Get sorted edges
                var sortedEdges = getEdges.toSeq.sorted

                // Map of usage counts
                val vertUse = new MutableHashMap[T, _bucket[T]]
                for (v <- indices) vertUse += (v -> new _bucket[T](v))
                var total_count = 0
                var edgeNum = 0

                // Connection groups
                val groups = new DisjointHashSet[_bucket[T]](
                    new HashSet[_bucket[T]]() ++ vertUse.values
                )

                var shuffled = false
                val numEdges = sortedEdges.size
                val shuffleAfter = (shuffle * numEdges).toInt

                // Skip input number of edges
                val leftBy = if (numEdges == 0) 0
                             else ((skip % numEdges) + numEdges) % numEdges
                sortedEdges = sortedEdges.drop(leftBy) ++ sortedEdges.take(leftBy)

                // Shuffle at input number
                sortedEdges = sortedEdges.take(shuffleAfter) ++
                                Random.shuffle(sortedEdges.drop(shuffleAfter))

                // Slowly add edges O(V^2)
                while (edgeNum < numEdges && total_count < indices.size)
                {
                    // Get next edge
                    val edge = sortedEdges(edgeNum)

                    // Get the buckets holding the vertices of the edge
                    val src = vertUse(edge.source)
                    val dst = vertUse(edge.destination)

                    // Check that the vertices have been used < 2 times,
                    val in_range = src.count < 2 && dst.count < 2
                    // Are not already in the same 'chain'
                    val not_loop = groups.find(src) != groups.find(dst)
                    // Or that it's the last connection needed
                    val is_final = total_count == indices.size - 1

                    if (in_range && (not_loop || is_final))
                    {
                        // Connect the bucket chains
                        src.connect(dst) // O(1)
                        dst.connect(src) // O(1)
                        groups.union(src, dst) // O(log V)

                        // Increment used vertex count
                        total_count += 1
                    }

                    // Increment considered edge count
                    edgeNum += 1
                }

                // Backtrack the chain
                var past = vertUse(indices.head)
                var current = past.head
                var s_verts = Seq.empty[T]

                for (i <- 0 to indices.size)
                {
                    s_verts = s_verts :+ current.vertex
                    val next = if (current.tail == past)
                                    current.head
                               else current.tail

                    past = current
                    current = next
                }
                getEdges(s_verts).toSeq
            }

            def getLocalTSP():Seq[Edge[T]] =
            {
                getBeamTSP(1)
            }

            def getLocalTSP(initialTour:Seq[T]):Seq[Edge[T]] =
            {
                getBeamTSP(initialTour, 1)
            }

            def getBeamTSP(beamSize:Int):Seq[Edge[T]] =
            {
                // Get random tour
                val tour = getRandomTour

                // Call given random tour
                getBeamTSP(tour :+ tour(0), beamSize)
            }

            def getBeamTSP(initialTour:Seq[T], beamSize:Int):Seq[Edge[T]] =
            {
                /**
                 * Recursive sub-function to perform 2-opt search on
                 *   upper beam-number of tours
                 */
                def recurse(tour:Seq[T], beam:Int, cost:Long):Seq[T] =
                {
                    // Sequence to hold flip points and weights
                    var costs = Seq.empty[((Int, Int), Int)]

                    // Iterate through all combinations of flippable vertices
                    for (end <- 4 until tour.length)
                    {
                        for (start <- 0 until end - 2)
                        {
                            // If the altered path exists, add it to the seq
                            val flipCost = calcFlipCost(tour, start, end)
                            if (flipCost != None)
                            {
                                costs = costs :+ ((start, end), flipCost.get)
                            }
                        }
                    }

                    // Get the top k-best swaps
                    //  (that are better than initial tour)
                    costs = costs.filter(_._2 < 0).sortBy(_._2).take(beam)

                    // If there are actually any improvements
                    if (!costs.isEmpty)
                    {
                        // Get all altered tours
                        val flips = costs.map(
                            (t) => (flipAt(tour, t._1._1, t._1._2), t._2)
                        )

                        // Recurse on each tour
                        val tours = flips.map(
                            (t) => (recurse(t._1, beam, cost + t._2), t._2)
                        )

                        // Return best tour
                        tours.minBy((t) => t._2)._1
                    }
                    // Return if no better tour exists
                    else tour
                }

                // Get best tour recursively
                val initialCost = pathLength(initialTour).get
                val tour = recurse(initialTour, beamSize, initialCost)

                // Convert tour to seq of edges
                getEdges(tour).toSeq
            }

            def dynamicTSP:Seq[Edge[T]] =
            {
                // Map for function lookups
                val dist = new MutableHashMap[(T, Set[T]), Int]()
                val parent = new MutableHashMap[(T, Set[T]), T]()

                /**
                 * Sub-function for adding vertex, subset pair to the
                 *  lookup maps.
                 */
                def addToMaps(vert:T, subset:Set[T]) =
                {
                    val options = for (v <- subset) yield
                    {
                        val wgt = getEdgeWeight(vert, v)
                        val dst = dist((v, subset - v))

                        // Pair vertex with weight
                        (v, wgt + dst)
                    }

                    // Pull the minimum
                    val min = options.minBy((t) => t._2)

                    // Add to the maps
                    dist += (((vert, subset), min._2))
                    parent += (((vert, subset), min._1))
                }

                // Starting vertex
                val v1 = indices.head
                val vertices = indices.tail

                // Set up base case
                for (vert <- vertices)
                {
                    dist += (((vert, Set.empty[T]), getEdgeWeight(v1, vert)))
                    parent += (((vert, Set.empty[T]), v1))
                }

                // Recursive cases
                for (k <- 1 until vertices.size)
                {
                    for (subset <- vertices.subsets(k))
                    {
                        for (vert <- vertices -- subset)
                        {
                            addToMaps(vert, subset)
                        }
                    }
                }

                // Final case
                addToMaps(v1, vertices)

                // Set up for backtracking
                var current = v1
                var verts = indices

                // Backtrack parent map
                val out = for (_ <- 0 until verts.size) yield
                {
                    verts = verts - current
                    val next = parent((current, verts))
                    val edge = getEdge(current, next).get
                    current = next

                    edge
                }

                // Return seqence of edges
                out.toSeq
            }

            def getGeneticTSP(
                popSize:Int, inversionProb:Float,
                maxIters:Int
            ):Seq[Edge[T]] =
            {
                // Collection of initial tours generated by 2-opt
                var population = for (i <- 0 to popSize) yield
                {
                    val tour = getLocalTSP.map((e) => e.source)
                    val score = pathLength(tour :+ tour(0)).get

                    (tour, score)
                }

                // Output Variable
                var bestTour = population.head

                // Random number generator
                val r = Random

                /**
                 * Sub-function for generating a random mutation.
                 */
                def mutate(c:Int):Int =
                {
                    // Return a random number not c
                    val num = r.nextInt(indices.size)
                    if (num < c) num else num + 1
                }

                /**
                 * Sub-function for finding a cross-over from a random
                 *  other tour.
                 */
                def crossOver(c:Int, tour:Seq[T]):Int =
                {
                    // Grab other tour
                    val tour1 = population(r.nextInt(population.size))._1

                    // Get index of neighbor in other tour
                    val ind = tour1.indexOf(tour(c)) + 1

                    if (ind < 0) println(c, tour(c), tour1.contains(tour(c)))

                    // Get neighbor in other tour
                    val neighbor = tour1(ind % tour1.size)

                    // Get index of neighbor in original tour
                    tour.indexOf(neighbor)
                }

                /**
                 * Sub-function for crossing over or mutating.
                 */
                def crossOrMutate(c:Int, tour:Seq[T]):Int =
                {
                    if (r.nextFloat < inversionProb) mutate(c)
                    else crossOver(c, tour)
                }

                /**
                 * Sub-function for swapping portions of the given tour
                 *  if it's cheaper to do so.
                 */
                def swap(tour:Seq[T], s:Int, e:Int):(Seq[T], Long) =
                {
                    val score = calcFlipCost(tour :+ tour(0), s, e)

                    if (score == None || score.get > 0) (tour, 0)
                    else (flipAt(tour, s, e), score.get)
                }

                // End condition setup
                var rounds = 0
                var roundsStable = 0
                val endCondition = 20

                /**
                 * Sub-function for checking if the end conditions have
                 *  been met.
                 */
                def notEnd():Boolean = (
                    rounds < maxIters ||
                    maxIters == -1) &&
                    roundsStable < endCondition

                /**
                 * Sub-function for checking the stability of the best
                 *  solution.
                 */
                def checkStable(tour:(Seq[T], Long)):(Seq[T], Long) =
                {
                    val best = population.minBy((p) => p._2)

                    if (best == tour) { roundsStable += 1; tour }
                    else { roundsStable = 0; best }
                }

                while (notEnd)
                {
                    rounds += 1

                    // Update each tour
                    population = population.map( (tour) => {
                        // Get current tour
                        var tour_ = tour

                        // Indexing variables
                        var c = r.nextInt(indices.size)
                        var c1 = crossOrMutate(c, tour_._1)

                        do {
                            // Alter the tour
                            tour_ = if (c >  c1) swap(tour_._1, c1, c)
                                    else         swap(tour_._1, c, c1)

                            // Get new Indices
                            c = c1 % tour_._1.size
                            c1 = crossOrMutate(c, tour_._1)
                        } while ((c1 != c + 1) && (c1 != c - 1))

                        // Return altered tour
                        (tour_._1, tour_._2 + tour._2)
                    })

                    bestTour = checkStable(bestTour)
                }

                getEdges(bestTour._1 :+ bestTour._1(0)).toSeq
            }

            def getGeneticTSP:Seq[Edge[T]] =
            {
                // Parameters
                val popSize = 5
                val inversionProb = 0.05f
                val maxIters = 20

                getGeneticTSP(popSize, inversionProb, maxIters)
            }

            def branchBoundTwo:Seq[Edge[T]] =
            {
                def isSolution(path:Seq[T]):Boolean =
                {
                    // Check size and elements
                    (path.size == indices.size)
                }

                var bestSolution = getLocalTSP.map((t) => t.source)
                bestSolution = bestSolution :+ bestSolution(0)
                var score = pathLength(bestSolution).get
                var start = bestSolution(0)
                var stack = Stack[Seq[T]]()
                stack.push(Seq(start))

                while (stack.nonEmpty)
                {
                    // Pop head of stack
                    val current = stack.pop

                    // Score it
                    val currentScore = pathLength(current).get

                    if (currentScore < score)
                    {
                        if (isSolution(current))
                        {
                            val currentSolution = current :+ current(0)
                            val currentScore = pathLength(currentSolution).get

                            if (currentScore < score)
                            {
                                score = currentScore
                                bestSolution = currentSolution
                            }
                        }
                        else
                        {
                            for (vertex <- indices -- current)
                            {
                                stack.push(current :+ vertex)
                            }
                        }
                    }
                }

                getEdges(bestSolution).toSeq
            }

            def branchBoundTSP:Seq[Edge[T]] =
            {
                var bestSolution = getLocalTSP.map((t) => t.source)
                bestSolution = bestSolution :+ bestSolution(0)
                var score = pathLength(bestSolution).get
                var start = bestSolution(0)
                var stack = Stack[(Seq[T], Long)]()
                stack.push((Seq(start), 0))

                while (stack.nonEmpty)
                {
                    // Pop head of stack
                    val current = stack.pop

                    // Split it
                    val cTour  = current._1
                    val cScore = current._2

                    if (cScore < score)
                    {
                        if (cTour.size == indices.size)
                        {
                            val nScore = cScore +
                                    getEdgeWeight(cTour(0), cTour.last)
                            val nTour = cTour :+ cTour(0)

                            if (nScore < score)
                            {
                                score = nScore
                                bestSolution = nTour
                            }
                        }
                        else
                        {
                            for (vertex <- indices -- cTour)
                            {
                                stack.push((cTour :+ vertex, cScore +
                                    getEdgeWeight(cTour.last, vertex)))
                            }
                        }
                    }
                }

                getEdges(bestSolution).toSeq
            }

            /**
             * Private function for claculating the decrease in tour cost
             *  of the given tour.
             *
             * Returns less than 0 if cost is 'better' (less) than initial
             */
            private[this] def calcFlipCost(tour:Seq[T], s:Int, e:Int):
                Option[Int] =
            {
                // Adjacent inner and outer vertices
                val r = (e - 1 + tour.size) % tour.size
                val l = (s + 1) % tour.size

                // Calc weights
                val normWeight  = getEdgeWeight(tour(s), tour(l)) +
                                  getEdgeWeight(tour(r), tour(e))
                val flipWeightS = getEdgeWeight(tour(s), tour(r))
                val flipWeightE = getEdgeWeight(tour(l), tour(e))

                // Check both edges exist
                if (flipWeightS > 0 && flipWeightE > 0)
                    Some(flipWeightE + flipWeightS - normWeight)
                else None
            }

            /**
             * Private function for inverting chunks of a tour
             */
            private[this] def flipAt(tour:Seq[T], s:Int, e:Int):Seq[T] =
            {
                // Section the tour
                val left = tour.take(s + 1)
                val right = tour.drop(e)
                val middle = tour.take(e).drop(s + 1)

                // Invert the middle section of the tour
                left ++ middle.reverse ++ right
            }

            /**
             * Private function used to generate a random tour of vertices
             */
            private[this] def getRandomTour():Seq[T] =
            {
                var tour = indices.toList

                do {
                    tour = Random.shuffle(tour)
                } while(!isValidTour(tour :+ tour(0)))

                tour
            }

            /**
             * Private function used in generating random tours
             */
            private[this] def isValidTour(tour:Seq[T]):Boolean =
            {
                // Check path exists
                val exists = (pathLength(tour) != None)

                // Check starts and ends at same vertex
                val closed = (tour(0).equals(tour(tour.length-1)))

                // Check contains all vertices
                val complete = indices.toSet.equals(tour.toSet)

                // Return "satisfies all the above"
                exists && closed && complete
            }

            override def toString:String =
            {
                "Graph(Directed: " + isDirected + ", Vertices: " +
                    indices.toIterable.mkString(", ") + ", Edges: " +
                    getEdges.toIterable.mkString(", ") + ")"
            }

            private[this] def containsNull(things:Any*):Boolean =
            {
                for (thing <- things)
                {
                    if (thing == null) return true
                }

                false
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureNotNull(things:Any*) =
            {
                for (thing <- things)
                {
                    if (thing == null)
                    {
                        throw new IllegalArgumentException(
                            "Null input not allowed"
                        );
                    }
                }
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureNotContainsVertex(vertex:T) =
            {
                if (indices.contains(vertex))
                {
                    throw new IllegalArgumentException(
                        "Contains vertex: " + vertex.toString
                    );
                }
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureContainsVertex(vertices:T*) =
            {
                for (vertex <- vertices)
                {
                    if (!indices.contains(vertex))
                    {
                        throw new IllegalArgumentException(
                            "Missing vertex: " + vertex.toString
                        );
                    }
                }
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureContainsEdge(source:T, destination:T) =
            {
                if (!edgeExists(source, destination))
                {
                    throw new IllegalArgumentException(
                        "Missing edge between: " + source + ", " + destination
                    );
                }
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureNotSame(vert1:T, vert2:T) =
            {
                if (vert1 == vert2)
                {
                    throw new IllegalArgumentException(
                        "Identical vertices not allowed"
                    );
                }
            }

            @throws(classOf[IllegalArgumentException])
            private[this] def ensureGreaterThanZero(nums:Int*) =
            {
                for (num <- nums)
                {
                    if (num <= 0)
                    {
                        throw new IllegalArgumentException(
                            "Negative numbers not allowed"
                        );
                    }
                }
            }
        }

        /**
         * Companion Object to the GraphEdge class for producing hashable
         *   edges for the graph class to use for lookups
         */
        private object GraphEdge
        {
            /**
             * Factory function for GraphEdge objects
             *
             * @return a GraphEdge that implements the equals method correctly
             *   dependent on the isDirected input
             */
            def apply[T](x:T, y:T, isDirected:Boolean):GraphEdge[T] =
            {
                if (isDirected) new DirectedEdge[T](x, y)
                else new UndirectedEdge[T](x, y)
            }
        }

        /**
         * Class which the GraphImpl class uses for hashable edge lookups
         */
        private abstract class GraphEdge[T](val x:T, val y:T)
        {
            override def equals(arg0:Any):Boolean

            override def hashCode():Int =
            {
                x.hashCode() * y.hashCode()
            }

            def contains(other:T):Boolean =
            {
                x.equals(other) || y.equals(other)
            }
        }

        /**
         * The undirected implementation of the GraphEdge class.
         */
        private class UndirectedEdge[T](x:T, y:T) extends GraphEdge[T](x, y)
        {
            override def equals(arg0:Any):Boolean =
            {
                if (arg0.isInstanceOf[GraphEdge[T]])
                {
                    // Cast
                    val that = arg0.asInstanceOf[GraphEdge[T]]

                    // Compare vertices
                    val backward = x.equals(that.y) && y.equals(that.x)
                    val forward  = x.equals(that.x) && y.equals(that.y)

                    // Return true if equal either direction
                    forward || backward
                }
                else false
            }
        }

        /**
         * The directed implementation fo the GraphEdge class.
         */
        private class DirectedEdge[T](x:T, y:T) extends GraphEdge[T](x, y)
        {
            override def equals(arg0:Any):Boolean =
            {
                if (arg0.isInstanceOf[GraphEdge[T]])
                {
                    // Cast
                    val that = arg0.asInstanceOf[GraphEdge[T]]

                    // Compare vertices
                    x.equals(that.x) && y.equals(that.y)
                }
                else false
            }
        }

        /**
         * A Disjoint Set class which uses hashable lookups to build a
         *   disjoint set of type `E`.
         */
        private class DisjointHashSet[E](val elements:HashSet[E])
        {
            // Set up group and height maps
            var groups = new MutableHashMap[E, E]
            var height = new MutableHashMap[E, Int]

            // Add each element to the height map
            for (elem <- elements)
            {
                height += ((elem, 0))
            }

            /**
             * Provides a label for the group in which the given element is.
             *
             * Does not ensure that the group label of an object will not change
             *   whenever a `union` is called.
             *
             * @return a label for the group in which the given element is
             */
            def find(elem:E):E =
            {
                // Validate input
                ensureContainsElement(elem)

                if (groups.get(elem) == None) elem
                else find(groups(elem))
            }

            /**
             * Joins the groups of the two given elements.
             *
             * Does not ensure that the group labels will remain the same as
             *   returned by `find`.
             */
            def union(elem1:E, elem2:E) =
            {
                // Validate input
                ensureContainsElement(elem1, elem2)

                // Get the root of each tree
                val root1 = find(elem1)
                val root2 = find(elem2)

                // Ensure not in the same tree
                if (root1 != root2)
                {
                    // Add smaller tree to larger
                    if (height(root1) > height(root2))
                    {
                        groups += ((root2, root1))
                    }
                    else if (height(root1) < height(root2))
                    {
                        groups += ((root1, root2))
                    }
                    else
                    {
                        // Case: same size
                        // add second tree to first and increment size
                        height += ((root1, height(root1) + 1))
                        groups += ((root1, root2))
                    }
                }
            }

            private[this] def ensureContainsElement(elems:E*) =
            {
                for (element <- elems)
                {
                    if (!this.elements.contains(element))
                    {
                        throw new NoSuchElementException("Missing: " + element.toString)
                    }
                }
            }
        }
    }
}
