
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.{TimeoutException => Timeout}
import scala.util.Random

object branchBoundTSPTests
{
    
    val burma11 = graph.Graph.fromTSPFile("tsp/burma11.xml")
    val burma12 = graph.Graph.fromTSPFile("tsp/burma12.xml")
    val burma13 = graph.Graph.fromTSPFile("tsp/burma13.xml")
    val burma14 = graph.Graph.fromTSPFile("tsp/burma14.xml")

    val burma10 = burma11.removeVertex(10)
    val burma9  = burma10.removeVertex(9)
    val burma8  = burma9.removeVertex(8)
    val burma7  = burma8.removeVertex(7)

    /*val ulysses16 = graph.Graph.fromTSPFile("tsp/ulysses16.xml")
    val ulysses15 = ulysses16.removeVertex(15)
    val ulysses14 = ulysses15.removeVertex(14)
    val ulysses13 = ulysses14.removeVertex(13)
    val ulysses12 = ulysses13.removeVertex(12)
    val ulysses11 = ulysses12.removeVertex(11)
    val ulysses10 = ulysses11.removeVertex(10)
    val ulysses9 = ulysses10.removeVertex(9)
    val ulysses8 = ulysses9.removeVertex(8)
    val ulysses7 = ulysses8.removeVertex(7)*/
    /*
    val ulysses22 = graph.Graph.fromTSPFile("tsp/ulysses22.xml")
    */


    val completeGraph = graph.Graph.fromCSVFile(true, "files/completeGraph.csv")

    def testFunction[T](name:String, runs:Int, gr:graph.Graph[T],
        f: graph.Graph[T] => Seq[graph.Edge[T]]):(Int, Long) =
    {
        def inner():(Int, Long) =
        {
            var start = System.currentTimeMillis
            val tour = f(gr)
            val end = System.currentTimeMillis
            val time = end - start

            val score = tour.map((e) => e.weight).sum


            (score, time)
        }

        val results = for (i <- 1 to runs) yield
        {
            //print("run " + i + "/" + runs + "\r")
            inner()
        }

        val score = results.map((t) => t._1).sum / runs
        val time  = results.map((t) => t._2).sum / runs

        val (time_, unit) = if (time < 10000) (time, "ms")
                       else if (time < 10000000)(time / 1000, "s")
                       else (time / 60000, "min")

        //print(name + "\tScore: " + score)
        //println("\tTime:  " + time_ + " " + unit)

        (score, time)
    }

    def testAll[T](name:String, runs:Int, gr:graph.Graph[T]) =
    {
        println()
        println("___________________________________________")
        println()
        println("Testing: " + name + " with " + gr.sizeVertices + " vertices")
        println()

        var grid = List.empty[(Int, Float, Int)]
        //for (k <- 1 to 5)
        //for (j <- 1 to 20)
          //  grid = grid :+ (20, (j * 0.002).toFloat, 20)

        /*
        for (t <- grid)
        {
            val name = t._1 + ", " + t._2 + ", " + t._3
            testFunction(name + " :    ", runs, gr,
                {g : graph.Graph[T] => g.getGeneticTSP(t._1, t._2, t._3)})
        }*/

        val opt = testFunction("B&B       " + name + " :\t", 1, gr,
            {g : graph.Graph[T] => g.branchBoundTSP})

        val min = testFunction("min       " + name + " :\t", runs, gr,
            {g : graph.Graph[T] => g.getKruskalTSP})
        println(math.round(-100 * (opt._1 - min._1) / opt._1))

        val loc = testFunction("local     " + name + " :\t", runs, gr,
            {g : graph.Graph[T] => g.getLocalTSP})
        println(math.round(-100 * (opt._1 - loc._1) / opt._1))

        val gen = testFunction("genetic   " + name + " :\t", runs, gr,
            {g : graph.Graph[T] => g.getGeneticTSP})
        println(math.round(-100 * (opt._1 - gen._1) / opt._1))



        //testFunction("B & B   :  ", runs, gr,
          //      {g : graph.Graph[T] => g.branchBoundTSP})

        //testFunction("Fast BB :  ", runs, gr,
          //      {g : graph.Graph[T] => g.branchBoundTwo})
    }

    def rand_diff(size:Int, runs:Int)
    {
        var list1 = Array.empty[Double]
        var list2 = Array.empty[Double]
        var list3 = Array.empty[Double]
        val name = "rand"
        var total1 = 0.0
        var total2 = 0.0
        var total3 = 0.0
        for (i <- 1 to runs)
        {
            val gr = randGraph(size)
            val opt = testFunction("B&B       " + name + " :\t", 1, gr,
                {g : graph.Graph[Int] => g.branchBoundTSP})

            val min1 = testFunction("min       " + name + " :\t", 1, gr,
                {g : graph.Graph[Int] => g.getKruskalTSP})
            val new_score1:Double = ((1.0 * min1._1) / (1.0 * opt._1))
            total1 = total1 + new_score1
            val avg1 = total1 / i
            list1 = list1 :+ (new_score1)

            val min2 = testFunction("min       " + name + " :\t", 1, gr,
                {g : graph.Graph[Int] => g.getKruskalTSP(skip=2, shuffle=0.6)})
            val new_score2:Double = ((1.0 * min2._1) / (1.0 * opt._1))
            total2 = total2 + new_score2
            val avg2 = total2 / i
            list2 = list2 :+ (new_score2)

            val loc1 = testFunction("loc       " + name + " :\t", 1, gr,
                {g : graph.Graph[Int] => g.getLocalTSP})
            val new_score3:Double = ((1.0 * loc1._1) / (1.0 * opt._1))
            total3 = total3 + new_score3
            val avg3 = total3 / i
            list3 = list3 :+ (new_score3)

            print(f"$i%4.0f avg1: $avg1%1.3f   |   avg2: $avg2%1.3f   |   avg3: $avg3%1.3f\r")
            System.gc()
        }
        println()
        println("min max: " + list1.max)
        println("mod max: " + list2.max)
        println("loc max: " + list3.max)
    }

    def main(args:Array[String])
    {
        val min = 60
        val hour = 60 * min

        val runs = 1

        rand_diff(12, 1000)

        /*
        testAll("ulysses7", 200, ulysses7)
        testAll("ulysses8", 200, ulysses8)
        testAll("ulysses9",  200, ulysses9)
        testAll("ulysses10", 200, ulysses10)
        testAll("ulysses11",  200, ulysses11)
        */
        /*
        testAll("burma7", 10 * runs, burma7)
        testAll("burma8", 10 * runs, burma8)
        testAll("burma9",  10 * runs, burma9)
        testAll("burma10", 10 * runs, burma10)

        testAll("burma11", runs,  burma11)
        testAll("burma12", runs,  burma12)
        testAll("burma13", runs,  burma13)
        testAll("burma14", runs,  burma14)*/

        /*testAll("ulysses16", runs, graph.Graph.fromTSPFile("ulysses16.xml"))
        //testAll("gr21", 10, graph.Graph.fromTSPFile("gr21.xml"))
        testAll("gr24", runs, graph.Graph.fromTSPFile("gr24.xml"))
        System.gc()
        testAll("bays29", runs, graph.Graph.fromTSPFile("bays29.xml"))
        System.gc()
        testAll("swiss42", runs, graph.Graph.fromTSPFile("swiss42.xml"))
        System.gc()
        testAll("eil76", runs, graph.Graph.fromTSPFile("eil76.xml"))
        System.gc()*/

        /*
        testAll("kroA100", runs, graph.Graph.fromTSPFile("kroA100.xml"))
        System.gc()
        testAll("tsp225", runs, graph.Graph.fromTSPFile("tsp225.xml"))
        System.gc()
        testAll("si535", runs, graph.Graph.fromTSPFile("si535.xml"))
        System.gc()
        testAll("u1817", runs, graph.Graph.fromTSPFile("u1817.xml"))
        System.gc()
        testAll("u2319", runs, graph.Graph.fromTSPFile("u2319.xml"))
        System.gc()*/

        /*testAll("gr21", 10, gr21)
        testAll("ulysses22", 10, ulysses22)
        testAll("gr24", 10, gr24)
        testAll("bays29", 10, bays29)*/
    }

    def randGraph(size:Int):graph.Graph[Int] =
    {
        var gr = graph.Graph[Int](false)
        for (i <- 0 until size) gr = gr.addVertex(i)
        val r = Random

        for (i <- 0 until size)
            for (j <- 0 until i)
                gr = gr.addEdge(i, j, r.nextInt(size) + 1)

        gr
    }

    @throws(classOf[java.util.concurrent.TimeoutException])
    def timedRun[F](timeout: Long)(f: => F): F =
    {
        import java.util.concurrent.{Callable, FutureTask, TimeUnit}

        val task = new FutureTask(new Callable[F]() {
            def call() = f
        })

        new Thread(task).start()

        task.get(timeout, TimeUnit.MILLISECONDS)
    }

}
