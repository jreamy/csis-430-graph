
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException

class EdgeTests extends org.scalatest.FunSuite
{
    val directed = false
    val dirGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    
    test("Add Edge Works")
    {
        assert(dirGraph.addEdge("A", "D", 4).edgeExists("A", "D"))
    }

    test("Add Edge Missing Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addEdge("A", "E", 1)
        }
    }

    test("Add Edge null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addEdge("A", null, 1)
        }
    }

    test("Add Edge Same Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addEdge("A", "A", 3)
        }
    }

    test("Add Edge Negative Weight")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addEdge("A", "D", -4)
        }
    }
    
    test("Add Edge Directed")
    {
        val g = dirGraph.addEdge("B", "A", 2)
        if (directed)
        {
            assert(g.getEdgeWeight("B", "A") === 2)
            assert(g.getEdgeWeight("A", "B") === 1)
        }
        else
        {
            assert(g.getEdgeWeight("A", "B") === 2)
        }
    }

    test("Remove Edge Works")
    {
        assert(!dirGraph.removeEdge("A", "B").edgeExists("A", "B"))
    }

    test("Remove Edge Missing Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.removeEdge("E", "A")
        }
    }

    test("Remove Edge null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.removeEdge("A", null)
        }
    }

    test("Remove Edge Missing Edge")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.removeEdge("A", "D")
        }
    }

    test("Remove Edge Directed")
    {
        if (directed)
        {
            assertThrows[IllegalArgumentException]
            {
                dirGraph.removeEdge("B", "A")
            }
        }
        else
        {
            assert(!dirGraph.removeEdge("B", "A").edgeExists("A", "B"))
        }
    }
    
    test("Edge Exists Works")
    {
        assert(dirGraph.edgeExists("A", "B"))
        assert(!dirGraph.edgeExists("A", "D"))
    }
    
    test("Edge Exists Missing Vertex")
    {
        assert(!dirGraph.edgeExists("A", "E"))
    }
    
    test("Edge Exists null Vertex")
    {
        assert(!dirGraph.edgeExists("A", null))
    }
    
    test("Get Edge Weight Works")
    {
        assert(dirGraph.getEdgeWeight("A", "C") === 3)
    }
    
    test("Get Edge Weight Missing Vertex")
    {
        assert(dirGraph.getEdgeWeight("A", "E") === -1)
    }
    
    test("Get Edge Weight Missing Edge")
    {
        assert(dirGraph.getEdgeWeight("A", "D") === -1)
    }
    
    test("Get Edge Weight Same Vertex")
    {
        assert(dirGraph.getEdgeWeight("A", "A") === -1)
    }
    
    test("Get Edge Weight null Vertex")
    {
        assert(dirGraph.getEdgeWeight("A", null) === -1)
    }
    
    test("Get Edge Weight Directed")
    {
        if (directed)
        {
            assert(dirGraph.getEdgeWeight("B", "A") === -1)
        }
        else
        {
            assert(dirGraph.getEdgeWeight("B", "A") === 1)
        }
    }

    test("Get Edges Works")
    {
        val edges = dirGraph.getEdges.toArray

        assert(edges.size === 3)
        assert(edges.contains(new graph.Edge[String]("A", "B", 1)))
        assert(edges.contains(new graph.Edge[String]("B", "C", 1)))
        assert(edges.contains(new graph.Edge[String]("A", "C", 3)))
        assert(!edges.contains(new graph.Edge[String]("A", "D", 2)))
    }
    
    test("Get Edges Empty Graph")
    {
        val g = Graph[String](directed)
        assert(g.getEdges.isEmpty)
    }
}