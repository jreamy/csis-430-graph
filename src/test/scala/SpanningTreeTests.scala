
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException

class SpanningTreeTests extends org.scalatest.FunSuite // with BeforeAndAfter
{
    val directedGraph = Graph[String](true).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    
    val disconnectedGraph = Graph[String](false).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    
    val bigGraph = Graph[String](false).addVertices(
        "A", "B", "C", "D", "E", "F", "G"
    ).addEdges(
        ("A", "B", 2), ("A", "D", 1), ("B", "D", 3), ("B", "E", 10),
        ("C", "A", 4), ("C", "F", 5), ("D", "C", 2), ("D", "F", 8),
        ("D", "G", 4), ("E", "G", 6), ("G", "F", 1), ("D", "E", 2)
    );
    
    test("Minimum Spanning Tree Works")
    {
        val tree = bigGraph.minimumSpanningTree.get
        
        assert(tree.edgeExists("A", "D"))
        assert(tree.edgeExists("F", "G"))
        assert(tree.edgeExists("D", "G"))
        assert(tree.edgeExists("D", "E"))
        assert(tree.edgeExists("C", "D"))
        
        assert(!tree.edgeExists("B", "D"))
        assert(!tree.edgeExists("B", "E"))
        assert(!tree.edgeExists("E", "G"))
        assert(!tree.edgeExists("D", "F"))
        assert(!tree.edgeExists("C", "F"))
        assert(!tree.edgeExists("A", "C"))
    }
    
    test("Minimum Spanning Tree Directed Graph")
    {
        assert(directedGraph.minimumSpanningTree === None)
    }
    
    test("Minimum Spanning Tree Disconnected Graph")
    {
        assert(disconnectedGraph.minimumSpanningTree === None)
    }

    test("Minimum Spanning Tree Empty Graph")
    {
        assert(Graph[String](false).minimumSpanningTree === None)
    }
}
