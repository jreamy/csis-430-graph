
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException

class VertexTests extends org.scalatest.FunSuite
{
    val directed = false
    val dirGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    
    test("Contains Correct Vertices")
    {
        assert(dirGraph.sizeVertices === 4)
        assert(dirGraph.vertexExists("A"))
        assert(dirGraph.vertexExists("B"))
        assert(dirGraph.vertexExists("C"))
        assert(dirGraph.vertexExists("D"))
    }
    
    test("Add Vertex Works")
    {
        assert(dirGraph.addVertex("E").vertexExists("E"))
    }

    test("Add Duplicate Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addVertex("A")
        }
    }

    test("Add null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.addVertex(null)
        }
    }

    test("Remove Works")
    {
        assert(!dirGraph.removeVertex("A").getVertices.toSeq.contains("A"))
    }

    test("Remove Missing Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.removeVertex("This doesn't exist")
        }
    }

    test("Remove null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.removeVertex(null)
        }
    }

    test("Vertex Exists Works")
    {
        assert(dirGraph.vertexExists("A"))
    }

    test("Vertex Exists Missing Vertex")
    {
        assert(!dirGraph.vertexExists("E"))
    }

    test("Vertex Exists null Vertex")
    {
        assert(!dirGraph.vertexExists(null))
    }
    
    test("Remove Vertex deletes edges")
    {
        assert(dirGraph.removeVertex("C").edgeExists("A", "B"))
        assert(!dirGraph.removeVertex("C").edgeExists("A", "C"))
    }
}