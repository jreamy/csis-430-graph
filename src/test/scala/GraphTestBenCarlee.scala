import org.scalatest._;
import graph.Graph;
import graph.Edge;

// Ben Garcia (bgarcia17@georgefox.edu)
// Unit tests for the new methods of the Graph trait.

class GraphTestBenCarlee extends FunSuite
{
    def buildTestGraph():Graph[String] = Graph[String](true).addVertex("Thing1").addVertex("Thing2").addVertex("Thing3").addVertex("Thing4").addVertex("Thing5").addEdge("Thing1", "Thing2", 10).addEdge("Thing1", "Thing4", 5).addEdge("Thing2", "Thing3", 7).addEdge("Thing3", "Thing4", 8).addEdge("Thing3", "Thing5", 2);
    def buildTestPath():Seq[String] = Seq[String]("Thing1", "Thing2", "Thing3");
    def buildTestPathBackwards():Seq[String] = Seq[String]("Thing1", "Thing2", "Thing3", "Thing4", "Thing1");
    def buildTestPathGap():Seq[String] = Seq[String]("Thing1", "Thing2", "Thing4");

    def testPath(g:Graph[String], p:Option[Seq[Edge[String]]], length:Long, vertices:String*) = {
        assert(!p.isEmpty);
        if (!p.isEmpty) {
            var len:Long = 0L;
            for (edge:Edge[String] <- p.get) {
                len += edge.weight;
            }
            assert(len == length);

            // TODO: Test against vertices
        }
    }

    test("edge-normal")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.getEdgeWeight("Thing1", "Thing2") == 10);
    }

    test("edge-reverse")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.getEdgeWeight("Thing2", "Thing1") == -1);
    }

    test("adjacent-null")
    {
        val g:Graph[String] = buildTestGraph;
        intercept[IllegalArgumentException]
        {
            val badVertices:Iterable[String] = g.getAdjacent(null);
        }
    }

    test("adjacent-outside")
    {
        val g:Graph[String] = buildTestGraph;
        intercept[IllegalArgumentException]
        {
            val badVertices:Iterable[String] = g.getAdjacent("Not In Graph");
        }
    }

    test("adjacent-none")
    {
        val g:Graph[String] = buildTestGraph;
        val adj:Iterable[String] = g.getAdjacent("Thing4");
        assert(adj.size == 0);
    }

    test("adjacent-out")
    {
        val g:Graph[String] = buildTestGraph;
        val adj:Iterable[String] = g.getAdjacent("Thing1");
        assert(adj.size == 2);
        assert(adj.head == "Thing2" && adj.tail.head == "Thing4" || adj.head == "Thing4" && adj.tail.head == "Thing2");
    }

    test("adjacent-both")
    {
        val g:Graph[String] = buildTestGraph;
        val adj:Iterable[String] = g.getAdjacent("Thing3");
        assert(adj.size == 2);
        assert(adj.head == "Thing5" && adj.tail.head == "Thing4" || adj.head == "Thing4" && adj.tail.head == "Thing5");
    }

    test("length-null")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.pathLength(null) === None);
    }

    test("length-none")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.pathLength(Seq[String]()) === None);
    }

    test("length-good")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.pathLength(buildTestPath()).get == 17L);
    }

    test("length-backwards")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.pathLength(buildTestPathBackwards).isEmpty);
    }

    test("length-gap")
    {
        val g:Graph[String] = buildTestGraph;
        assert(g.pathLength(buildTestPathGap).isEmpty);
    }

    test("shortest-1")
    {
        val g:Graph[String] = buildTestGraph;
        // TODO: Specify the exact expected path for testing
        testPath(g, g.shortestPathBetween("Thing1", "Thing2"), 10);
        testPath(g, g.shortestPathBetween("Thing1", "Thing4"), 5);
        testPath(g, g.shortestPathBetween("Thing1", "Thing3"), 17);
    }

    // IllegalArgumentException should be thrown when any of the vertices are null.
    test("shortest-null")
    {
        val g:Graph[String] = buildTestGraph;
        intercept[IllegalArgumentException]
        {
            g.shortestPathBetween("Thing1", null);
        }
    }

    // IllegalArgumentException should be thrown when any of the vertices aren't in the graph.
    test("shortest-outside")
    {
        val g:Graph[String] = buildTestGraph;
        intercept[IllegalArgumentException]
        {
            g.shortestPathBetween("Thing1", "ThingNotInTheGraph");
        }
    }
}