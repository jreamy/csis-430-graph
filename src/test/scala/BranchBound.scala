
import org.scalatest.FunSuite
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time.SpanSugar._
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException
import scala.util.Random.shuffle

class BranchBound extends FunSuite // with TimeLimitedTests with BeforeAndAfter
{
    val directed = false
    val dirGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    val bigGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D", "E", "F", "G"
    ).addEdges(
        ("A", "B", 2), ("A", "D", 1), ("B", "D", 3), ("B", "E", 10),
        ("C", "A", 4), ("C", "F", 5), ("D", "C", 2), ("D", "F", 8),
        ("D", "G", 4), ("E", "G", 6), ("G", "F", 1)
    );
    val denseGraph = Graph.fromCSVFile(directed, "files/denseGraph.csv")

    val completeGraph = Graph.fromCSVFile(true, "files/completeGraph.csv")
    
    test("Branch and Bound Works")
    {
        val tour = completeGraph.branchBoundTSP 
        assert(tour.size === completeGraph.sizeVertices)
    }

    test("Branch and Bound Continuous Path")
    {
        val tour = completeGraph.branchBoundTSP

        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("Branch and Bound Finds Cheapest Path")
    {
        for (i <- 0 until 10)
            assert(completeGraph.branchBoundTSP.map((e) => e.weight).sum === 51)
    }
}
