
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException

class GraphTest extends org.scalatest.FunSuite // with BeforeAndAfter
{
    val directed = false
    val dirGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    val bigGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D", "E", "F", "G"
    ).addEdges(
        ("A", "B", 2), ("A", "D", 1), ("B", "D", 3), ("B", "E", 10),
        ("C", "A", 4), ("C", "F", 5), ("D", "C", 2), ("D", "F", 8),
        ("D", "G", 4), ("E", "G", 6), ("G", "F", 1)
    );
    
    val filePath = "files/"
    
    test("Is immutable")
    {
        dirGraph.addVertex("E")
        
        assert(!dirGraph.vertexExists("E"))
    }

    test("Is Directed Works")
    {
        assert(dirGraph.isDirected == directed)
    }

    test("Get Adjacent Works")
    {
        assert(dirGraph.getAdjacent("A").exists(_.equals("B")))
        assert(dirGraph.getAdjacent("A").exists(_.equals("C")))
        assert(!dirGraph.getAdjacent("A").exists(_.equals("D")))
        assert(!dirGraph.getAdjacent("A").exists(_.equals("A")))
    }

    test("Get Adjacent Missing Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.getAdjacent("E")
        }
    }

    test("Get Adjacent null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            dirGraph.getAdjacent(null)
        }
    }
    
    def tryLoad(fileName:String):Graph[String] =
    {
        Graph.fromCSVFile(directed, filePath + fileName + ".csv")
    }
    
    test("From CSV Works")
    {
        tryLoad("tinyGraph")
    }
    
    test("From CSV Bad First Int")
    {
        assertThrows[IOException] 
        {
            tryLoad("badFirstInt")
        }
    }
    
    test("From CSV Bad Second Int")
    {
        assertThrows[IOException] 
        {
            tryLoad("badSecondInt")
        }
    }
    
    test("From CSV Missing Vertex")
    {
        assertThrows[IOException] 
        {
            tryLoad("missingVertex")
        }
    }
    
    test("From CSV Bad Vertex")
    {
        assertThrows[IOException] 
        {
            tryLoad("badVertex")
        }
    }
    
    test("From CSV Missing Vertex From Edge")
    {
        assertThrows[IOException] 
        {
            tryLoad("missingVertexFromEdge")
        }
    }
    
    test("From CSV Bad Edge Weight")
    {
        assertThrows[IOException] 
        {
            tryLoad("badEdgeWeight")
        }
    }
    
    test("From CSV Catch Trailing")
    {
        assertThrows[IOException] 
        {
            tryLoad("trailing")
        }
    }
}
