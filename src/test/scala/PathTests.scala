
import org.scalatest.FunSuite
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time.SpanSugar._
import org.scalatest.BeforeAndAfter
import graph.Graph
import java.io.IOException
import scala.util.Random.shuffle

class PathTests extends FunSuite with TimeLimitedTests// with BeforeAndAfter
{
    val directed = false
    val dirGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D"
    ).addEdges(
        ("A", "B", 1), ("B", "C", 1), ("A", "C", 3)
    );
    val bigGraph = Graph[String](directed).addVertices(
        "A", "B", "C", "D", "E", "F", "G"
    ).addEdges(
        ("A", "B", 2), ("A", "D", 1), ("B", "D", 3), ("B", "E", 10),
        ("C", "A", 4), ("C", "F", 5), ("D", "C", 2), ("D", "F", 8),
        ("D", "G", 4), ("E", "G", 6), ("G", "F", 1)
    );
    val denseGraph = Graph.fromCSVFile(directed, "files/denseGraph.csv")

    val completeGraph = Graph.fromCSVFile(true, "files/completeGraph.csv")

    val timeLimit = 400 millis
    
    test("Path Length Works")
    {
        assert(dirGraph.pathLength(List("A", "C")).get === 3)
        assert(dirGraph.pathLength(List("A", "B", "C")).get === 2)
    }

    test("Path Length Broken Path")
    {
        assert(dirGraph.pathLength(List("A", "D")) === None)
    }

    test("Path Length Directed")
    {
        if (directed) assert(dirGraph.pathLength(List("C", "A")) === None)
        else assert(dirGraph.pathLength(List("C", "A")).get === 3)
    }

    test("Shortest Path Between Works")
    {
        var path = bigGraph.shortestPathBetween("A", "F").get

        assert(path(0) === new graph.Edge("A", "D", 1))
        assert(path(1) === new graph.Edge("D", "G", 4))
        assert(path(2) === new graph.Edge("G", "F", 1))

        path = bigGraph.shortestPathBetween("B", "G").get

        assert(path(0) === new graph.Edge("B", "D", 3))
        assert(path(1) === new graph.Edge("D", "G", 4))
    }

    test("Shortest Path Between Missing Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            bigGraph.shortestPathBetween("A", "Z")
        }
    }

    test("Shortest Path Between null Vertex")
    {
        assertThrows[IllegalArgumentException]
        {
            bigGraph.shortestPathBetween("A", null)
        }
    }

    test("Shortest Path Between Disconnected Vertices")
    {
        assert(bigGraph.addVertex("Z").shortestPathBetween("A", "Z") === None)
    }

    test("Shortest Path Between Directed")
    {
        val path = bigGraph.shortestPathBetween("C", "B").get

        if (directed)
        {
            assert(path(0) === new graph.Edge("C", "A", 4))
            assert(path(1) === new graph.Edge("A", "B", 2))
        }
        else
        {
            assert(path(0) === new graph.Edge("C", "D", 2))
            assert(path(1) === new graph.Edge("D", "B", 3))
        }
    }
    
    test("Min TSP Works")
    {
        val tour = denseGraph.getMinTSP

        assert(tour.size === denseGraph.sizeVertices)
    }

    test("Min TSP Continuous Path")
    {
        val tour = denseGraph.getMinTSP

        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("Min TSP Finds Cheap Path")
    {
        assert(denseGraph.getMinTSP.map((e) => e.weight).sum < 53)
    }

    test("TSP Works")
    {
        val tour = denseGraph.getLocalTSP

        assert(tour.size === denseGraph.sizeVertices)
    }

    test("TSP Continuous Path")
    {
        val tour = denseGraph.getLocalTSP

        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("TSP Finds Cheap Path")
    {
        assert(denseGraph.getLocalTSP.map((e) => e.weight).sum < 53)
    }

    test("TSP Works Given Tour")
    {
        val tour = Seq("A", "C", "B", "F", "E", "D", "A")

        val tsp = denseGraph.getLocalTSP(tour)

        assert(tsp.size === tour.size - 1)
    }

    test("TSP Continuous Path Given Tour")
    {
        val init = Seq("A", "C", "B", "F", "E", "D", "A")
        val tour = denseGraph.getLocalTSP(init)

        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("TSP Finds Cheaper Path Given Tour")
    {
        val tour = Seq("A", "C", "B", "F", "E", "D", "A")
        val cost1 = denseGraph.pathLength(tour).get

        val tsp = denseGraph.getLocalTSP(tour)
        val cost2 = tsp.map((e) => e.weight).sum

        assert(cost2 < cost1)
    }

    def isValidTour[T](gr:graph.Graph[T], tour:Seq[T]):Boolean =
    {
        gr.pathLength(tour) != None && (tour(0).equals(tour(tour.length-1)))
    }

    def getRandomTour[T](gr:graph.Graph[T]):Seq[T] =
    {
        var tour = gr.getVertices.toList

        while(!isValidTour[T](gr, tour :+ tour(0)))
        {
            tour = shuffle(tour)
        }

        tour :+ tour(0)
    }

    test("Beam TSP Works")
    {
        for (i <- 1 until 10)
        {
            assert(denseGraph.getBeamTSP(i).size === denseGraph.sizeVertices)
        }
    }

    test("Beam TSP Continuous Path")
    {
        for (i <- 1 until 10)
        {
            val tour = denseGraph.getBeamTSP(i)

            for (pair <- tour.sliding(2))
            {
                assert(pair(0).destination === pair(1).source)
            }

            assert(tour(0).source === tour(tour.size - 1).destination)
        }
    }

    test("Beam TSP Finds Cheap Path")
    {
        for (i <- 1 until 10)
        {
            assert(denseGraph.getBeamTSP(i).map((e) => e.weight).sum < 53)
        }
    }

    test("Beam TSP Works Given Tour")
    {
        for (i <- 1 until 10)
        {
            for (j <- 1 until 10)
            {
                val tour = getRandomTour[String](denseGraph)
                assert(denseGraph.getBeamTSP(tour, i).size === denseGraph.sizeVertices)
            }
        }
    }

    test("Beam TSP Continuous Given Tour")
    {
        for (i <- 1 until 10)
        {
            for (j <- 1 until 10)
            {
                val tour = denseGraph.getBeamTSP(getRandomTour[String](denseGraph), i)

                for (pair <- tour.sliding(2))
                {
                    assert(pair(0).destination === pair(1).source)
                }

                assert(tour(0).source === tour(tour.size - 1).destination)
            }
        }
    }

    test("Beam TSP Cheaper Given Tour")
    {
        for (i <- 1 until 10)
        {
            for (j <- 1 until 10)
            {
                val tour = getRandomTour[String](denseGraph)
                assert(denseGraph.getBeamTSP(tour, i).map((e) => e.weight).sum < 53)
            }
        }
    }

    test("Dynamic TSP Works")
    {
        val tour = completeGraph.dynamicTSP

        assert(tour.size === denseGraph.sizeVertices)
    }

    test("Dynamic TSP Continuous Path")
    {
        val tour = completeGraph.dynamicTSP

        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("Dynamic TSP Finds Cheap Path")
    {
        assert(completeGraph.dynamicTSP.map((e) => e.weight).sum === 51)
    }

    test("Genetic TSP Works")
    {
        val tour1 = completeGraph.getGeneticTSP
        val tour2 = denseGraph.getGeneticTSP

        assert(tour1.size === completeGraph.sizeVertices)
        assert(tour2.size === denseGraph.sizeVertices)
    }
    
    test("Genetic TSP Continuous Path")
    {
        val tour = denseGraph.getGeneticTSP
        
        for (pair <- tour.sliding(2))
        {
            assert(pair(0).destination === pair(1).source)
        }

        assert(tour(0).source === tour(tour.size - 1).destination)
    }

    test("Genetic TSP Finds Cheap Path")
    {
        val tour1 = completeGraph.getGeneticTSP
        val tour2 = denseGraph.getGeneticTSP

        assert(tour1.map((e) => e.weight).sum < 65)
        assert(tour2.map((e) => e.weight).sum < 53)
    }

    test("Genetic TSP 1 Tour")
    {
        val popSize = 1
        val inversionProb = 0.5f
        val maxIters = 20

        val tour = completeGraph.getGeneticTSP(
            popSize, inversionProb, maxIters
        )//._1

        assert(tour.size === completeGraph.sizeVertices)
    }

    test("Genetic TSP Always Mutate")
    {
        val popSize = 5
        val inversionProb = .99f
        val maxIters = 20

        val tour = completeGraph.getGeneticTSP(
            popSize, inversionProb, maxIters
        )//._1

        assert(tour.size === completeGraph.sizeVertices)
    }

    test("Genetic TSP Always Cross-Over")
    {
        val popSize = 5
        val inversionProb = 0.01f
        val maxIters = 20

        val tour = completeGraph.getGeneticTSP(
            popSize, inversionProb, maxIters
        )//._1

        assert(tour.size === completeGraph.sizeVertices)
    }

    test("Genetic TSP No Iterations")
    {
        val popSize = 5
        val inversionProb = 0.5f
        val maxIters = 0

        val tour = completeGraph.getGeneticTSP(
            popSize, inversionProb, maxIters
        )

        assert(tour.size === completeGraph.sizeVertices)
    }
    
    test("Genetic TSP Infinite Run")
    {
        val popSize = 5
        val inversionProb = 0.5f
        val maxIters = -1

        val tour = completeGraph.getGeneticTSP(
            popSize, inversionProb, maxIters
        )

        assert(tour.size === completeGraph.sizeVertices)
        assert(tour.map((e) => e.weight).sum < 65)
    }
}
